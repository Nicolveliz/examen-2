﻿using System;
using System.Collections.Generic;
using System.Text;

namespace patron_ADAPTER
{
    class ADAPTADoR:ITarget
    {
        //en este metodo se va a adaptar la multiplicacion para relizarlo como un array
        CALCULAR_ARRAY C = new CALCULAR_ARRAY();
        public int Multiplicar(int Num1, int Num2)
        {
            double Result=0;

            int[] operador = {Num1,Num2 };

            Result = C.Multi(operador);

            return (int)Result;
        }

        
    }
}

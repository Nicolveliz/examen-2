﻿using System;

namespace patron_ADAPTER
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //creamos una variable tipo ITarget
            ITarget Calculadora = new Calcular();

            //ingrresamos los numeros a multiplicar 
            Console.WriteLine("ingrese el 1er numero: ");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("ingrese el 2do numero: ");
            int num2 = int.Parse(Console.ReadLine());
            //multiplicamos 
            int Resultado=Calculadora.Multiplicar(num1,num2);

            Console.WriteLine("LA MULTIPLICACION ES: "+Resultado);

            

            Calculadora = new ADAPTADoR();//adaptamos el metodo de la multiplicacion 
Console.WriteLine("ingrese el 1er numero: ");
            num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("ingrese el 2do numero: ");
            num2 = int.Parse(Console.ReadLine());
            Resultado = Calculadora.Multiplicar(num1,num2);//multiplicamos
            Console.WriteLine("EL RESULTADO ES: " + Resultado);

        }
    }
}

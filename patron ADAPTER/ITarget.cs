﻿using System;
using System.Collections.Generic;
using System.Text;

namespace patron_ADAPTER
{
    interface ITarget
    {
        //interfaz que obliga a realizar el metodo a las clases que lo implenten
        int Multiplicar(int Num1, int Num2);
    }
}

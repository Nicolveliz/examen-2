﻿using System;
using System.Collections.Generic;
using System.Text;

namespace patron_ADAPTER
{
    class CALCULAR_ARRAY
    {

        //en esta clase recibimos un array para poder multiplicar cada una de sus posiciones
        public double Multi(int[] array)
        {
            int Array=1;
            for (int i = 0; i < array.Length; i++)
            {
                Array *= array[i];        
            }

            return Array;
        }

    }
}
